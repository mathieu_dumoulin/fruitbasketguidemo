package ca.ulaval.ift.gloo.view;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

import ca.ulaval.ift.gloo.guidemo.model.FruitBasketController;
import ca.ulaval.ift.gloo.guidemo.model.Strawberry;

public class DisplayFruitPanel extends JPanel implements MouseInputListener,
		MouseMotionListener {
	private static final long serialVersionUID = 1L;

	private final DisplayModel display;
	private final FruitBasketController controller;

	private static final String APPLE_IMAGE_PATH = "/apple.png";
	private static final String BANANA_IMAGE_PATH = "/banana.png";
	private static final String IMAGE_STRAWBERRY_PATH = "/strawberry.png";

	private boolean hasSelectedStrawberry = false;
	private Strawberry selectedStrawberry = null;

	private Image strawberryImage = null;

	public DisplayFruitPanel(FruitBasketController basket) throws IOException {
		this.controller = basket;

		Image appleImage = null;
		Image bananaImage = null;
		appleImage = ImageIO.read(getClass().getResourceAsStream(
				APPLE_IMAGE_PATH));
		bananaImage = ImageIO.read(getClass().getResourceAsStream(
				BANANA_IMAGE_PATH));
		strawberryImage = ImageIO.read(getClass().getResourceAsStream(
				IMAGE_STRAWBERRY_PATH));
		this.display = new DisplayModel(appleImage, bananaImage,
				strawberryImage);

		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		display.paintModel(g, controller);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int width = strawberryImage.getWidth(null);
		int height = strawberryImage.getHeight(null);
		if (!controller.hasStrawberryAt(e.getX() - width / 2, e.getY() - height
				/ 2, width, height)) {
			controller.addStrawberry(e.getPoint().x, e.getPoint().y);
			repaint();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		display.setMousePosition(e.getPoint());
		repaint();
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// DO NOTHING OK!
	}

	@Override
	public void mousePressed(MouseEvent e) {
		int width = strawberryImage.getWidth(null);
		int height = strawberryImage.getHeight(null);
		selectedStrawberry = controller.getStrawberryAt(e.getX() - width / 2,
				e.getY() - height / 2, width, height);
		if (selectedStrawberry != null) {
			hasSelectedStrawberry = true;
			controller.moveStrawberry(selectedStrawberry, e.getX() - 5,
					e.getY() + 5);
			repaint();
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		if (hasSelectedStrawberry) {
			hasSelectedStrawberry = false;
			selectedStrawberry = null;
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (hasSelectedStrawberry) {
			controller.moveStrawberry(selectedStrawberry, e.getX(), e.getY());
			repaint();
		}

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		display.setMousePosition(e.getPoint());
		repaint();
	}
}
