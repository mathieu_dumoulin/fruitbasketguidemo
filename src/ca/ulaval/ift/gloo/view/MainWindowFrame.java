package ca.ulaval.ift.gloo.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JFrame;

import ca.ulaval.ift.gloo.guidemo.model.FruitBasketController;

@SuppressWarnings("serial")
public class MainWindowFrame extends JFrame {
	private final DisplayFruitPanel fruitImagesPanel;
	private final CreateFruitsControlerPanel createFruitPanel;
	
	public MainWindowFrame(FruitBasketController basket) throws IOException {
		super("Demonstration du GUI avec Afficheur");
		setLayout(new BorderLayout());
		
		fruitImagesPanel = new DisplayFruitPanel(basket);
		add(fruitImagesPanel, BorderLayout.CENTER);

		createFruitPanel = new CreateFruitsControlerPanel(basket, fruitImagesPanel);
		add(createFruitPanel, BorderLayout.WEST);
		

		setPreferredSize(new Dimension(800, 400));
		setLocation(300, 200);

		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}
