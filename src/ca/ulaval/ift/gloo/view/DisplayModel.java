package ca.ulaval.ift.gloo.view;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;

import ca.ulaval.ift.gloo.guidemo.model.FruitBasketController;
import ca.ulaval.ift.gloo.guidemo.model.Strawberry;

public class DisplayModel {

	private static final int BORDER_SIZE = 10;

	private static final int APPLE_INITIAL_Y_POSITION = 0;
	private static final int BANANA_INITIAL_Y_POSITION = 100;

	private static final int Y_STEP_SIZE = 10;
	private static final int X_STEP_SIZE = 50;

	private Image appleImage = null;
	private Image bananaImage = null;
	private Image strawberryImage = null;
	
	private String message = "Click in the window for a sweet surprise!";

	private Point mousePosition = new Point(0, 0);

	public DisplayModel(Image appleImage, Image bananaImage,
			Image strawberryImage) {
		this.appleImage = appleImage;
		this.bananaImage = bananaImage;
		this.strawberryImage = strawberryImage;
	}

	public void paintModel(Graphics g, FruitBasketController basket) {		
		g.setFont(g.getFont().deriveFont(20.0f));
		if (!basket.getStrawberries().isEmpty()) {
			message = "You can drag the strawberries around with your mouse, too!";
		} 
		g.drawString(message, 50, 40);
		g.drawString("" + mousePosition.x + ":" + mousePosition.y, 0, 18);

		if (appleImage != null && bananaImage != null) {
			int x = BORDER_SIZE;
			int y = BORDER_SIZE + APPLE_INITIAL_Y_POSITION;
			for (int i = 0; i < basket.getApples().size(); i++) {
				g.drawImage(appleImage, x, y, null);
				x += X_STEP_SIZE;
				y += Y_STEP_SIZE;
			}
			x = BORDER_SIZE;
			y = BORDER_SIZE + BANANA_INITIAL_Y_POSITION;
			for (int i = 0; i < basket.getBananas().size(); i++) {
				g.drawImage(bananaImage, x, y, null);
				x += 50;
				y += 10;
			}

			for (Strawberry s : basket.getStrawberries()) {
				int sbX = s.getX() - strawberryImage.getWidth(null) / 2;
				int sbY = s.getY() - strawberryImage.getHeight(null) / 2;
				g.drawImage(strawberryImage, sbX, sbY, null);
				g.drawString("" + sbX + ":" + sbY, sbX, sbY);
			}
		}
	}

	public void setMousePosition(Point point) {
		mousePosition = point;

	}
}
