package ca.ulaval.ift.gloo.guidemo;

import java.io.IOException;

import ca.ulaval.ift.gloo.guidemo.model.FruitBasketController;
import ca.ulaval.ift.gloo.view.MainWindowFrame;

public class Main {
	public static void main(String[] args) {
		try {
			new MainWindowFrame(new FruitBasketController());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
