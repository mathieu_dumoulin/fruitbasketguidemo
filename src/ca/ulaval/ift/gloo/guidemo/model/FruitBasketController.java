package ca.ulaval.ift.gloo.guidemo.model;

import java.util.ArrayList;
import java.util.List;

public class FruitBasketController {
	public List<Apple> apples = new ArrayList<>();
	public List<Banana> bananas = new ArrayList<>();
	public List<Strawberry> strawberries = new ArrayList<>();

	public List<Apple> getApples() {
		return apples;
	}

	public List<Banana> getBananas() {
		return bananas;
	}

	public List<Strawberry> getStrawberries() {
		return strawberries;
	}

	public void addApples(int numberToCreate) {
		for (int i = 0; i < numberToCreate; i++) {
			addApple(new Apple());
		}
	}

	public void addBananas(int numberToCreate) {
		for (int i = 0; i < numberToCreate; i++) {
			addBanana(new Banana());
		}
	}

	public void addApple(Apple pomme) {
		apples.add(pomme);
	}

	public void addBanana(Banana banane) {
		bananas.add(banane);
	}

	public void addStrawberry(int x, int y) {
		strawberries.add(new Strawberry(x, y));
	}

	public Strawberry getStrawberryAt(int x, int y, int width, int height) {
		for (Strawberry s : strawberries) {
			if (s.getX() >= x && s.getX() < x + width && s.getY() >= y
					&& s.getY() < y + height) {
				return s;
			}
		}
		return null;
	}

	public void moveStrawberry(Strawberry s, int x, int y) {
		s.setPosition(x, y);
	}

	public boolean hasStrawberryAt(int x, int y, int width, int height) {
		return getStrawberryAt(x, y, width, height) != null;
	}
}
