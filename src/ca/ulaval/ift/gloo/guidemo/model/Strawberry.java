package ca.ulaval.ift.gloo.guidemo.model;

public class Strawberry implements Fruit {
	private int x;
	private int y;

	public Strawberry(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setPosition(int newX, int newY) {
		x = newX;
		y = newY;
	}
}
